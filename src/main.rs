#[macro_use] extern crate clap;
extern crate thiserror;
extern crate serde;
extern crate serde_yaml;
extern crate base64;

use std::{
    convert::TryFrom,
    collections::HashMap,
    error::Error,
    fs,
    io,
    path::PathBuf,
    process,
};
use serde::{
    ser::{Serialize, Serializer},
};
use serde_yaml::Value;

#[derive(Debug, clap::Parser)]
#[clap(name = crate_name!(), version = crate_version!(), author = crate_authors!(), about = crate_description!())]
struct Args {
    files: Vec<PathBuf>,
}

impl Args {
    fn streams(&self) -> io::Result<Vec<Box<dyn io::Read>>> {
        if self.files.is_empty() {
            Ok(vec![Box::new(io::stdin())])
        } else {
            let mut ret: Vec<Box<dyn io::Read>> = Vec::with_capacity(self.files.len());
            let it = self.files.iter()
                .map(|p| fs::OpenOptions::new().read(true).open(p));
            for f in it {
                let f = f?;
                ret.push(Box::new(f) as _);
            }
            Ok(ret)
        }
    }

    fn run(self) -> Result<(), Box<dyn Error + 'static>> {
        for s in self.streams()? {
            let secret = serde_yaml::from_reader::<_, serde_yaml::Mapping>(s)
                .map_err(KubernetesSecretParseError::from)
                .and_then(KubernetesSecret::try_from)?;
            serde_yaml::to_writer(io::stdout(), &secret)?;
        }
        Ok(())
    }
}

#[derive(Debug, thiserror::Error)]
enum KubernetesSecretParseError {
    #[error("Error parsing kubernetes secret: {0}")]
    Parse(#[from] serde_yaml::Error),
    #[error("Error decoding secret key {key}: {error}")]
    Decode {
        key: String,
        error: base64::DecodeError,
    },
    #[error("Invalid utf8 value for secret key {key}: {error}")]
    InvalidUtf8 {
        key: String,
        error: std::string::FromUtf8Error,
    }
}

#[derive(Debug, Clone)]
struct KubernetesSecret {
    data: serde_yaml::Mapping,
    values: HashMap<String, String>,
}

impl KubernetesSecret {
    fn values_mapping(&self) -> serde_yaml::Mapping {
        self.values.iter().map(|(k, v)| (k.clone().into(), Value::String(v.clone()))).collect()
    }
}

impl TryFrom<serde_yaml::Mapping> for KubernetesSecret {
    type Error = KubernetesSecretParseError;
    fn try_from(mut data: serde_yaml::Mapping) -> Result<Self, KubernetesSecretParseError> {
        let mut values = if let Some(v) = data.remove("stringData") {
            serde_yaml::from_value::<HashMap<String, String>>(v)?
        } else {
            HashMap::new()
        };
        if let Some(Value::Mapping(encoded_values)) = data.remove("data") {
            let mut decoded_values: Vec<(String, String)> = Vec::with_capacity(encoded_values.len());
            for (k, v) in encoded_values.into_iter() {
                let k: String = serde_yaml::from_value(k)?;
                let v: String = serde_yaml::from_value(v)?;
                let value_bytes = base64::decode(&v)
                    .map_err(|e| KubernetesSecretParseError::Decode {
                        key: k.clone(),
                        error: e,
                    })?;
                let value = String::from_utf8(value_bytes)
                    .map_err(|e| KubernetesSecretParseError::InvalidUtf8 {
                        key: k.clone(),
                        error: e,
                    })?;
                decoded_values.push((k, value));
            }
            values.extend(decoded_values.into_iter());
        }
        Ok(KubernetesSecret {
            data: data,
            values: values,
        })
    }
}

impl Serialize for KubernetesSecret {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        // TODO: more effient to borrow the data as we go
        let mut full_data = self.data.clone();
        full_data.insert(Value::String("stringData".into()), Value::Mapping(self.values_mapping()));
        full_data.serialize(serializer)
    }
}

fn main() {
    use clap::Parser;
    let config = Args::parse();
    if let Err(e) = config.run() {
        eprintln!("{}", &e);
        process::exit(1);
    }
}
